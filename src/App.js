import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    firstName: "",
    lastName: "",
    submissionData: false,
  }
  handleChange(event) {
    let { name, value } = event.target;
    if (name === "firstName") {
      this.setState({
        firstName: value
      })
    } else if (name === "lastName") {
      this.setState({
        lastName: value
      })
    }else if (name === "address") {
      this.setState({
        address: value
      })
    }
    else if (name === "phone") {
      this.setState({
        phone: value
      })
    }
    else if (name === "email") {
      this.setState({
        email: value
      })
    }
  }

  handleSubmit() {
    this.setState({
      submissionData: true
    })
  }
  render() {
    return (
      <div>
        <h1>Form Handling</h1>
        <div>
          <label>First Name: </label>
          <input
            type="text"
            name="firstName"
            value={this.state.firstName}
            onChange={(event) => { this.handleChange(event) }}
          />
        </div>
        <div>
          <label>Last Name:</label>
          <input
            type="text"
            name="lastName"
            value={this.state.lastName}
            onChange={(event) => { this.handleChange(event) }}
          />
        </div>
        <div>
          <label>Address:</label>
          <input
            type="text"
            name="address"
            value={this.state.address}
            onChange={(event) => { this.handleChange(event) }}
          />
        </div>
        <div>
          <label>Phone:</label>
          <input
            type="number"
            name="phone"
            value={this.state.phone}
            onChange={(event) => { this.handleChange(event) }}
          />
        </div>
        <div>
          <label>Email:</label>
          <input
            type="email"
            name="email"
            value={this.state.email}
            onChange={(event) => { this.handleChange(event) }}
          />
        </div>
        <div>
          <button onClick={() => { this.handleSubmit() }} >Submit</button>
        </div>
        {this.state.submissionData === true ?
          <p>
            Your Name is : {this.state.firstName} {this.state.lastName}
            <p>Your Address is: {this.state.address}</p>
            <p>Your Phone Number is: {this.state.phone}</p>
            <p>Your Email is: {this.state.email}</p>
            </p>
  : ""

}
      </div>
    )
  }
}
export default App;
